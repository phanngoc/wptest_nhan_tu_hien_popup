﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using PhoneApp4.Resources;

using System.Text.RegularExpressions;
namespace PhoneApp4
{
    public partial class MainPage : PhoneApplicationPage
    {

        public MainPage()
        {
            InitializeComponent();

            webrowser.IsScriptEnabled = true;
            webrowser.IsGeolocationEnabled = true;
            webrowser.Navigating += Browser_Navigating;
            string test = "A nurse's assistant contracts Ebola after helping treat two victims repatriated to Spain from Africa. Unions allege inadequate equipment was used.";

            String webtest = Regex.Replace(test, @"([\w\']+)[\s+\.]", delegate(Match match)
            {
                string v = match.Groups[1].Value;
                return " <a href=\""+v+"\">" + v + "</a> ";
            });
            System.Diagnostics.Debug.WriteLine(webtest);
            webtest = @"<html><head><style>a{color:black;}</style></head><body>"+webtest+"</body></html>";
            webrowser.NavigateToString(webtest);
            //webrowser.Navigated += new EventHandler<NavigationEventArgs>(webBrowser1_Navigated);

        }
       // private void webBrowser1_Navigated(object sender, NavigatedEventArgs e)
       // {
       //     toolStripTextBox1.Text = webBrowser1.Url.ToString();
       // }
        void Browser_Navigating(object sender, NavigatingEventArgs e) 
        {
            MessageBox.Show(e.Uri.ToString().Substring(6), "Từ vừa ấn:", MessageBoxButton.OK);
            e.Cancel = true;
            //System.Diagnostics.Debug.WriteLine("navigating:");
        } 
        
    }
}